﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Securework.View
{
	public partial class MainPage : ContentPage
	{
        Image splashImage;

        public MainPage()
		{
            

            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            var sub = new AbsoluteLayout();

            splashImage = new Image
            {
                Source = "inicio.png"

            };
            AbsoluteLayout.SetLayoutFlags(splashImage,
            AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(splashImage,
             new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            sub.Children.Add(splashImage);

            this.BackgroundColor = Color.FromHex("#6DC00C");
            this.Content = sub;

        }
        protected override async void
            OnAppearing()
        {
            base.OnAppearing();

            await splashImage.ScaleTo(1, 2000); //Time-consuming processes such as initialization
            await splashImage.ScaleTo(0.9, 1500, Easing.Linear);
            await splashImage.ScaleTo(150, 1200, Easing.Linear);
            Application.Current.MainPage = new NavigationPage(new Home());    //After loading  MainPage it gets Navigated to our new Page
        }

    }
}
